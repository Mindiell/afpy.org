VENV = $(PWD)/venv
PIP = $(VENV)/bin/pip
PYTHON = $(VENV)/bin/python
FLASK = $(VENV)/bin/flask
ISORT = $(VENV)/bin/isort
BLACK = $(VENV)/bin/black

.PHONY: all
all: install serve

.PHONY: install
install:
	test -d $(VENV) || python3 -m venv $(VENV)
	$(PIP) install --upgrade --no-cache pip setuptools -r requirements.txt -r requirements-dev.txt

.PHONY: clean
clean:
	rm -fr $(VENV)

.PHONY: check-outdated
check-outdated:
	$(PIP) list --outdated --format=columns

.PHONY: test
test:
	$(PYTHON) -m flake8 afpy
	$(PYTHON) -m pytest tests.py afpy/ --black --cov=afpy --cov=tests --cov-report=term-missing

.PHONY: serve
serve:
	$(PYTHON) run.py

.PHONY: publish
publish:
	# To get the right to publish, add your public key in the nginx_public_deploy_key here:
	# https://git.afpy.org/AFPy/infra/src/branch/main/afpy.org.yml
	rsync -a ./ afpy-org@deb2.afpy.org:/home/afpy-org/src/
	ssh afpy-org@deb2.afpy.org '\
	    /home/afpy-org/venv/bin/python -m pip install --upgrade setuptools wheel pip; \
	    /home/afpy-org/venv/bin/python -m pip install -r /home/afpy-org/src/requirements.txt; \
	    /home/afpy-org/venv/bin/python -m pip install sentry-sdk[flask]; \
	    sudo systemctl restart afpy-org.service; \
	'
